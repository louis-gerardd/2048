# 2048
## Louis Gerard Productions


---

### Le projet

Il s'agit d'un programme servant à comparer deux IA jouant à 2048 afin de déterminer laquelle fait un meilleure score et laquelle est donc la plus performante. 

### Jouer !

Pour jouer, l'IA devra écrire son coup dans le fichier player1.move ou player2.move (faites s'il vous plaît en sorte qu'on puisse choisir facilement) et attendre que le programme lui rende la nouvelle grille avant de jouer son tour suivant. 

### Contrôles ;)

Elle devra écrire **h** pour faire glisser en haut, **b** pour bas, **g** pour gauche et **d** pour droite. Tout autre caractère entraînera la disqualification de l'IA. 
Il est très important d'attendre d'avoir la réponse du programme avant de jouer, sinon le coup sera ignoré. 
Dans le fichier de retour, la matrice est modélisée de cette manière : `2-0-0-0|0-0-0-0|0-0-0-0|4-0-2-0`. Les nombres représentent les cases, séparées par des tirets et les pipes pour les retours à la ligne. 
Vous avez la permission de récupérer tout ce dont vous avez besoin dans les sources (dans /src/) notamment la fonction Grid::move(char direction); pour anticiper les coups. 
Cela dit, chercher une IA existante sur Internet (ou où que ce soit d'ailleurs) est évidemment interdit. Participez avec fair play s'il vous play.
Le langage conseillé est le c++ pour le lien avec le cours mais vous pouvez en choisir un autre. 

### Conseils et astuces :D

Nous vous conseillons de commencer jouer au jeu pour en dégager une stratégie. 
Nous sommes à votre disposition si vous avez des questions ou si vous rencontrez un problème qui vous bloquent trop longtemps, le but étant que chacun ait sa création fonctionnelle pour qu’il y ait du spectacle !
