#-------------------------------------------------
#
# Project created by QtCreator 2015-07-24T01:12:55
#
#-------------------------------------------------

#debug console
CONFIG += console

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2048
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    grid.cpp \
    tile.cpp \
    score.cpp \
    player.cpp

HEADERS  += mainwindow.h \
    grid.h \
    tile.h \
    score.h \
    player.h

FORMS    += mainwindow.ui
