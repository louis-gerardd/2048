#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTime>
#include "tile.h"
#include "player.h"   //Grid

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    unsigned getPauseTime() const;

//debug:
    void debug();

public slots:
    void fileChangedSlot(const QString & path);
    void timerSlot();

private:
    void linkUiCases(std::vector< std::vector<Tile> > & matrix1, std::vector< std::vector<Tile> > & matrix2);
    Ui::MainWindow *ui;
    Player m_player1, m_player2;
    QTime timeJ1, timeJ2;
    bool playedJ1 = false;
    bool playedJ2 = false;
    bool lostJ1 = false;
    bool lostJ2 = false;
    char directionJ1, directionJ2;
    unsigned pauseTime = 500;
};

#endif // MAINWINDOW_H
