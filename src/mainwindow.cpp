#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QThread>
#include <vector>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    std::vector< std::vector<Tile> > matrix1;
    std::vector< std::vector<Tile> > matrix2;
    linkUiCases(matrix1, matrix2);

    Score score1;
    Score score2;
    score1.linkUI(ui->score1);
    score2.linkUI(ui->score1_2);

    Grid grid1(matrix1, score1);
    Grid grid2(matrix2, score2);

    m_player1.initialize(grid1, QString("player1.return"), ui->J1, statusBar());
    m_player2.initialize(grid2, QString("player2.return"), ui->J2, statusBar());

    timeJ1.start();
    timeJ2.start();

    statusBar()->showMessage("Ready");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::fileChangedSlot(const QString & path)
{
    // Initialisation fichier source
    QFile dataSource (path);
    if (!dataSource.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(this, "Erreur de lecture", "Nous n'avons pas réussi à lire les informations.");
        std::exit(1);
    }

    // Lecture du fichier
    QTextStream in(&dataSource);
    char data;
    QString line = QString(in.readLine());
    int attempts = 0;
    while(line.length() == 0 && attempts++ <= 5) {
        line = QString(in.readLine());
        qDebug() << "test" << attempts << "el : " << timeJ1.elapsed()<< "\n";
        QThread::msleep(100);
    }
    data = line.toStdString()[0];
    qDebug() << "got " << data << "\n";
    dataSource.flush();
    dataSource.close();

    qDebug() << "path : " << path << " elapsed : " << timeJ1.elapsed() << "\n";

    if( path == "player1.move" && timeJ1.elapsed() >= pauseTime )
    {
        playedJ1 = true;
        directionJ1 = data;
    }
    else if ( path == "player2.move" && timeJ2.elapsed() >= pauseTime )
    {
        playedJ2 = true;
        directionJ2 = data;
    }
}

void MainWindow::timerSlot()
{
    if(!lostJ1 && playedJ1)
    {
        lostJ1 = m_player1.play(directionJ1);
        timeJ1.restart();
        playedJ1 = false;
    }

    if(!lostJ2 && playedJ2)
    {
        lostJ2 = m_player2.play(directionJ2);
        timeJ2.restart();
        playedJ2 = false;
    }
}

void MainWindow::linkUiCases(std::vector< std::vector<Tile> > & matrix1, std::vector< std::vector<Tile> > & matrix2)
{
    std::vector<Tile> currentLine;

    //g1
    currentLine.push_back(ui->g1_case_0_0);
    currentLine.push_back(ui->g1_case_1_0);
    currentLine.push_back(ui->g1_case_2_0);
    currentLine.push_back(ui->g1_case_3_0);
    matrix1.push_back(currentLine);
    currentLine.erase(currentLine.begin(), currentLine.end());
    currentLine.push_back(ui->g1_case_0_1);
    currentLine.push_back(ui->g1_case_1_1);
    currentLine.push_back(ui->g1_case_2_1);
    currentLine.push_back(ui->g1_case_3_1);
    matrix1.push_back(currentLine);
    currentLine.erase(currentLine.begin(), currentLine.end());
    currentLine.push_back(ui->g1_case_0_2);
    currentLine.push_back(ui->g1_case_1_2);
    currentLine.push_back(ui->g1_case_2_2);
    currentLine.push_back(ui->g1_case_3_2);
    matrix1.push_back(currentLine);
    currentLine.erase(currentLine.begin(), currentLine.end());
    currentLine.push_back(ui->g1_case_0_3);
    currentLine.push_back(ui->g1_case_1_3);
    currentLine.push_back(ui->g1_case_2_3);
    currentLine.push_back(ui->g1_case_3_3);
    matrix1.push_back(currentLine);
    currentLine.erase(currentLine.begin(), currentLine.end());

    //g2
    currentLine.push_back(ui->g2_case_0_0);
    currentLine.push_back(ui->g2_case_1_0);
    currentLine.push_back(ui->g2_case_2_0);
    currentLine.push_back(ui->g2_case_3_0);
    matrix2.push_back(currentLine);
    currentLine.erase(currentLine.begin(), currentLine.end());
    currentLine.push_back(ui->g2_case_0_1);
    currentLine.push_back(ui->g2_case_1_1);
    currentLine.push_back(ui->g2_case_2_1);
    currentLine.push_back(ui->g2_case_3_1);
    matrix2.push_back(currentLine);
    currentLine.erase(currentLine.begin(), currentLine.end());
    currentLine.push_back(ui->g2_case_0_2);
    currentLine.push_back(ui->g2_case_1_2);
    currentLine.push_back(ui->g2_case_2_2);
    currentLine.push_back(ui->g2_case_3_2);
    matrix2.push_back(currentLine);
    currentLine.erase(currentLine.begin(), currentLine.end());
    currentLine.push_back(ui->g2_case_0_3);
    currentLine.push_back(ui->g2_case_1_3);
    currentLine.push_back(ui->g2_case_2_3);
    currentLine.push_back(ui->g2_case_3_3);
    matrix2.push_back(currentLine);
}
unsigned MainWindow::getPauseTime() const
{
    return pauseTime;
}

