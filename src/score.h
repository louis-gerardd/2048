#ifndef SCORE_H
#define SCORE_H

#include <QLabel>

class Score
{
private:
    unsigned m_value = 0;
    QLabel *m_uiObject;

public:
    void add(unsigned points);
    void linkUI(QLabel* & uiObject);
    Score();
};

#endif // SCORE_H
