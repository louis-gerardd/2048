#include "grid.h"
#include <stdlib.h> //rand()
#include <QDebug>

Grid::Grid(std::vector< std::vector<Tile> > & matrix, Score score) : m_matrix(matrix), m_score(score)
{
    //Create 2 tiles at the beginning
    addRandomTile();
    addRandomTile();
}

void Grid::addRandomTile()
{
    unsigned value = rand()%11 > 9 ? 4 : 2 ;

    if(!isFull())
    {
        unsigned x, y;
        do
        {
            x = rand()% m_matrix.size();
            y = rand()% m_matrix[x].size();
        } while(m_matrix[x][y].getValue() != 0);
        m_matrix[x][y].setValue(value);
    }
}

bool Grid::isFull()
{
    for (unsigned i = 0; i < m_matrix.size(); ++i)
        for (unsigned j = 0; j < m_matrix[i].size(); ++j)
            if (m_matrix[i][j].getValue() == 0)
                return false;
    return true;
}

bool Grid::isFusionnable()
{
    for (unsigned i = 0; i < m_matrix.size(); ++i)
        for (unsigned j = 0; j < m_matrix[i].size(); ++j)
            if ((i-1 < m_matrix.size() && m_matrix[i][j] == m_matrix[i-1][j])
                    ||
                (j-1 < m_matrix[i].size() && m_matrix[i][j] == m_matrix[i][j-1]))
                return true;
    return false;
}

bool Grid::move(char direction) throw()
{
    int x = 0;
    int y = 0;
    switch(direction)
    {
    case 'h' :
        y = -1;
        break;
    case 'b' :
        y = 1;
        break;
    case 'g' :
        x = -1;
        break;
    case 'd' :
        x = 1;
        break;
    default :
        throw "Mauvais mouvement";
    }

    bool fusion = false;
    bool played = false;
    if(direction == 'h' || direction == 'g')
        for(unsigned i = 0; i < m_matrix.size(); ++i)
        {
            for(unsigned j = 0; j < m_matrix[i].size(); ++j)
                if(moveTile(i, j, x, y, fusion))
                    played = true;
            fusion = false;
        }
    else
        for(unsigned i = m_matrix.size()-1; i+1 >= 1 ; --i)
        {
            for(unsigned j = m_matrix[i].size()-1; j+1 >= 1 ; --j)
                if(moveTile(i, j, x, y, fusion))
                    played = true;
            fusion = false;
        }
    return played;
}

bool Grid::moveTile(unsigned i, unsigned j, int x, int y, bool & fusion)
{
    bool played = false;
    bool moved = false;

    if(x != 0) std::swap(i, j);

    if( (int)(i)+x >= 0 && (int)(i)+x < (int)(m_matrix.size()) &&
        (int)(j)+y >= 0 && (int)(j)+y < (int)(m_matrix[i+x].size()) && m_matrix[i][j].getValue() != 0 )
    {
        int tempI = i+x;
        int tempJ = j+y;
        while( tempI >= 0 && tempI < (int)(m_matrix.size()) &&
               tempJ >= 0 && tempJ < (int)(m_matrix[tempI].size()) &&
               m_matrix[tempI][tempJ] == 0 )
        {
            m_matrix[tempI][tempJ].setValue(m_matrix[tempI-x][tempJ-y].getValue());
            m_matrix[tempI-x][tempJ-y].setValue(0);
            tempI += x;
            tempJ += y;
            played = true;
            moved = true;
        }

        if( tempI >= 0 && tempI < (int)(m_matrix.size()) &&
               tempJ >= 0 && tempJ < (int)(m_matrix[tempI].size()) &&
               m_matrix[tempI][tempJ] == m_matrix[tempI-x][tempJ-y] &&
               m_matrix[tempI][tempJ].getValue() != 0 )
        {
            if(!fusion)
            {
                m_matrix[tempI][tempJ].setValue( 2 * m_matrix[tempI][tempJ].getValue() );
                m_matrix[tempI-x][tempJ-y].setValue(0);
                m_score.add(m_matrix[tempI][tempJ].getValue());
                fusion = true;
                moved = false;
                played = true;
            }
            else
                fusion = false;
        }

        if(moved)
            fusion = false;
    }
    return played;
}

Grid::Grid() {}

void Grid::printDebug()
{
    QString output;
    for(unsigned i = 0; i < m_matrix.size(); ++i)
    {
        for(unsigned j = 0; j < m_matrix[i].size(); ++j)
            output += QString::number(m_matrix[j][i].getValue()) + " - ";
        qDebug() << output;
        output = "";
    }

}

void Grid::addTile(unsigned x, unsigned y, unsigned value /*=0*/)
{
    m_matrix[y][x].setValue(value);
}

void Grid::empty()
{
    for(unsigned i = 0; i < m_matrix.size(); ++i)
        for(unsigned j = 0; j < m_matrix[i].size(); ++j)
            m_matrix[i][j].setValue(0);
}

QString Grid::getMatrixStr()
{
    QString output;
    for(unsigned i = 0; i < m_matrix.size(); ++i)
    {
        for(unsigned j = 0; j < m_matrix[i].size(); ++j)
            output += QString::number(m_matrix[j][i].getValue()) + "-";
        output[output.size() - 1] = '|';
    }
    output.resize(output.size() - 1);
    return output;
}
