#include "tile.h"
#include <QLabel>
#include <math.h>   //pow()

#include <QDebug>

const unsigned Tile::COLORS[] = { 0xcdc1b4, //0
                                  0xeee4da, //2
                                  0xede0c8, //4
                                  0xf2b179, //8
                                  0xf59563, //16
                                  0xf67c5f, //32
                                  0xf65e3b, //64
                                  0xedcf72, //128
                                  0xedcc61, //256
                                  0xedc850, //512
                                  0xedc53f, //1024
                                  0xedc22e, //2048
                                  0x4d4a42 };//else

Tile::Tile(QLabel* & uiObject) : m_uiObject(uiObject), m_value(0) {}

void Tile::setValue(unsigned value)
{
    m_value = value;

    if(value != 0)
    {
        //set text
        m_uiObject->setText(QString::number(value));

        //set background
        unsigned newColorIndex = sizeof(COLORS)-1;
        if (value == 0)
            newColorIndex = 0;
        else
            for (unsigned i = 1; i < sizeof(COLORS); ++i)
                if(value == pow(2, i))
                {
                    newColorIndex = i;
                    break;
                }

        //set text color
        QString textColor = value >= 8 ? "white" : "black";

        m_uiObject->setStyleSheet("color: "+textColor+" ; " +
                                  "background: #"+QString::number(COLORS[newColorIndex], 16)+" ; ");
    }
    else    //empty tile
    {
        m_uiObject->setText("");
        m_uiObject->setStyleSheet("color: white; background: #" + QString::number(COLORS[0], 16) + " ; ");
    }
}

unsigned Tile::getValue()
{
    return m_value;
}

bool Tile::operator ==(Tile t2)
{
    return m_value == t2.m_value;
}

bool Tile::operator ==(unsigned val2)
{
    return m_value == val2;
}
