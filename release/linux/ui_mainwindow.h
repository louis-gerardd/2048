/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *J1;
    QLabel *J2;
    QWidget *container1;
    QWidget *gridLayoutWidget;
    QGridLayout *grid1;
    QLabel *g1_case_0_0;
    QLabel *g1_case_0_1;
    QLabel *g1_case_0_2;
    QLabel *g1_case_1_0;
    QLabel *g1_case_0_3;
    QLabel *g1_case_1_2;
    QLabel *g1_case_1_1;
    QLabel *g1_case_1_3;
    QLabel *g1_case_2_0;
    QLabel *g1_case_2_1;
    QLabel *g1_case_2_2;
    QLabel *g1_case_2_3;
    QLabel *g1_case_3_0;
    QLabel *g1_case_3_1;
    QLabel *g1_case_3_2;
    QLabel *g1_case_3_3;
    QWidget *container2;
    QWidget *gridLayoutWidget_3;
    QGridLayout *grid2;
    QLabel *g2_case_0_0;
    QLabel *g2_case_0_1;
    QLabel *g2_case_0_2;
    QLabel *g2_case_1_0;
    QLabel *g2_case_0_3;
    QLabel *g2_case_1_2;
    QLabel *g2_case_1_1;
    QLabel *g2_case_1_3;
    QLabel *g2_case_2_0;
    QLabel *g2_case_2_1;
    QLabel *g2_case_2_2;
    QLabel *g2_case_2_3;
    QLabel *g2_case_3_0;
    QLabel *g2_case_3_1;
    QLabel *g2_case_3_2;
    QLabel *g2_case_3_3;
    QLabel *score1;
    QLabel *score1_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(850, 492);
        MainWindow->setAutoFillBackground(false);
        MainWindow->setStyleSheet(QLatin1String("QWidget#container1 { background: #bbada0 }\n"
"QWidget#container2 { background: #bbada0 }\n"
"QLabel[case=\"true\"] { background : #cdc1b4 }\n"
"QWidget#centralWidget { background: #faf8ef }"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        J1 = new QLabel(centralWidget);
        J1->setObjectName(QStringLiteral("J1"));
        J1->setGeometry(QRect(0, 10, 425, 20));
        QFont font;
        font.setPointSize(15);
        J1->setFont(font);
        J1->setAlignment(Qt::AlignCenter);
        J2 = new QLabel(centralWidget);
        J2->setObjectName(QStringLiteral("J2"));
        J2->setGeometry(QRect(425, 10, 425, 20));
        J2->setFont(font);
        J2->setAlignment(Qt::AlignCenter);
        container1 = new QWidget(centralWidget);
        container1->setObjectName(QStringLiteral("container1"));
        container1->setGeometry(QRect(62, 50, 301, 301));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(container1->sizePolicy().hasHeightForWidth());
        container1->setSizePolicy(sizePolicy);
        container1->setStyleSheet(QStringLiteral(""));
        gridLayoutWidget = new QWidget(container1);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 301, 301));
        grid1 = new QGridLayout(gridLayoutWidget);
        grid1->setSpacing(5);
        grid1->setContentsMargins(11, 11, 11, 11);
        grid1->setObjectName(QStringLiteral("grid1"));
        grid1->setSizeConstraint(QLayout::SetDefaultConstraint);
        grid1->setContentsMargins(5, 5, 5, 5);
        g1_case_0_0 = new QLabel(gridLayoutWidget);
        g1_case_0_0->setObjectName(QStringLiteral("g1_case_0_0"));
        QFont font1;
        font1.setFamily(QStringLiteral("Generica"));
        font1.setPointSize(19);
        font1.setBold(true);
        font1.setWeight(75);
        g1_case_0_0->setFont(font1);
        g1_case_0_0->setAutoFillBackground(false);
        g1_case_0_0->setAlignment(Qt::AlignCenter);
        g1_case_0_0->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_0_0, 0, 0, 1, 1);

        g1_case_0_1 = new QLabel(gridLayoutWidget);
        g1_case_0_1->setObjectName(QStringLiteral("g1_case_0_1"));
        g1_case_0_1->setFont(font1);
        g1_case_0_1->setAutoFillBackground(false);
        g1_case_0_1->setAlignment(Qt::AlignCenter);
        g1_case_0_1->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_0_1, 0, 1, 1, 1);

        g1_case_0_2 = new QLabel(gridLayoutWidget);
        g1_case_0_2->setObjectName(QStringLiteral("g1_case_0_2"));
        g1_case_0_2->setFont(font1);
        g1_case_0_2->setAutoFillBackground(false);
        g1_case_0_2->setAlignment(Qt::AlignCenter);
        g1_case_0_2->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_0_2, 0, 2, 1, 1);

        g1_case_1_0 = new QLabel(gridLayoutWidget);
        g1_case_1_0->setObjectName(QStringLiteral("g1_case_1_0"));
        g1_case_1_0->setFont(font1);
        g1_case_1_0->setAutoFillBackground(false);
        g1_case_1_0->setAlignment(Qt::AlignCenter);
        g1_case_1_0->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_1_0, 1, 0, 1, 1);

        g1_case_0_3 = new QLabel(gridLayoutWidget);
        g1_case_0_3->setObjectName(QStringLiteral("g1_case_0_3"));
        g1_case_0_3->setFont(font1);
        g1_case_0_3->setAutoFillBackground(false);
        g1_case_0_3->setAlignment(Qt::AlignCenter);
        g1_case_0_3->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_0_3, 0, 3, 1, 1);

        g1_case_1_2 = new QLabel(gridLayoutWidget);
        g1_case_1_2->setObjectName(QStringLiteral("g1_case_1_2"));
        g1_case_1_2->setFont(font1);
        g1_case_1_2->setAutoFillBackground(false);
        g1_case_1_2->setAlignment(Qt::AlignCenter);
        g1_case_1_2->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_1_2, 1, 2, 1, 1);

        g1_case_1_1 = new QLabel(gridLayoutWidget);
        g1_case_1_1->setObjectName(QStringLiteral("g1_case_1_1"));
        g1_case_1_1->setFont(font1);
        g1_case_1_1->setAutoFillBackground(false);
        g1_case_1_1->setAlignment(Qt::AlignCenter);
        g1_case_1_1->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_1_1, 1, 1, 1, 1);

        g1_case_1_3 = new QLabel(gridLayoutWidget);
        g1_case_1_3->setObjectName(QStringLiteral("g1_case_1_3"));
        g1_case_1_3->setFont(font1);
        g1_case_1_3->setAutoFillBackground(false);
        g1_case_1_3->setAlignment(Qt::AlignCenter);
        g1_case_1_3->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_1_3, 1, 3, 1, 1);

        g1_case_2_0 = new QLabel(gridLayoutWidget);
        g1_case_2_0->setObjectName(QStringLiteral("g1_case_2_0"));
        g1_case_2_0->setFont(font1);
        g1_case_2_0->setAutoFillBackground(false);
        g1_case_2_0->setAlignment(Qt::AlignCenter);
        g1_case_2_0->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_2_0, 2, 0, 1, 1);

        g1_case_2_1 = new QLabel(gridLayoutWidget);
        g1_case_2_1->setObjectName(QStringLiteral("g1_case_2_1"));
        g1_case_2_1->setFont(font1);
        g1_case_2_1->setAutoFillBackground(false);
        g1_case_2_1->setAlignment(Qt::AlignCenter);
        g1_case_2_1->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_2_1, 2, 1, 1, 1);

        g1_case_2_2 = new QLabel(gridLayoutWidget);
        g1_case_2_2->setObjectName(QStringLiteral("g1_case_2_2"));
        g1_case_2_2->setFont(font1);
        g1_case_2_2->setAutoFillBackground(false);
        g1_case_2_2->setAlignment(Qt::AlignCenter);
        g1_case_2_2->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_2_2, 2, 2, 1, 1);

        g1_case_2_3 = new QLabel(gridLayoutWidget);
        g1_case_2_3->setObjectName(QStringLiteral("g1_case_2_3"));
        g1_case_2_3->setFont(font1);
        g1_case_2_3->setAutoFillBackground(false);
        g1_case_2_3->setAlignment(Qt::AlignCenter);
        g1_case_2_3->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_2_3, 2, 3, 1, 1);

        g1_case_3_0 = new QLabel(gridLayoutWidget);
        g1_case_3_0->setObjectName(QStringLiteral("g1_case_3_0"));
        g1_case_3_0->setFont(font1);
        g1_case_3_0->setAutoFillBackground(false);
        g1_case_3_0->setAlignment(Qt::AlignCenter);
        g1_case_3_0->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_3_0, 3, 0, 1, 1);

        g1_case_3_1 = new QLabel(gridLayoutWidget);
        g1_case_3_1->setObjectName(QStringLiteral("g1_case_3_1"));
        g1_case_3_1->setFont(font1);
        g1_case_3_1->setAutoFillBackground(false);
        g1_case_3_1->setAlignment(Qt::AlignCenter);
        g1_case_3_1->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_3_1, 3, 1, 1, 1);

        g1_case_3_2 = new QLabel(gridLayoutWidget);
        g1_case_3_2->setObjectName(QStringLiteral("g1_case_3_2"));
        g1_case_3_2->setFont(font1);
        g1_case_3_2->setAutoFillBackground(false);
        g1_case_3_2->setAlignment(Qt::AlignCenter);
        g1_case_3_2->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_3_2, 3, 2, 1, 1);

        g1_case_3_3 = new QLabel(gridLayoutWidget);
        g1_case_3_3->setObjectName(QStringLiteral("g1_case_3_3"));
        g1_case_3_3->setFont(font1);
        g1_case_3_3->setAutoFillBackground(false);
        g1_case_3_3->setAlignment(Qt::AlignCenter);
        g1_case_3_3->setProperty("case", QVariant(true));

        grid1->addWidget(g1_case_3_3, 3, 3, 1, 1);

        g1_case_3_0->raise();
        g1_case_1_0->raise();
        g1_case_3_1->raise();
        g1_case_2_0->raise();
        g1_case_0_0->raise();
        g1_case_0_1->raise();
        g1_case_2_1->raise();
        g1_case_0_3->raise();
        g1_case_2_3->raise();
        g1_case_3_2->raise();
        g1_case_1_3->raise();
        g1_case_2_2->raise();
        g1_case_0_2->raise();
        g1_case_1_2->raise();
        g1_case_1_1->raise();
        g1_case_3_3->raise();
        container2 = new QWidget(centralWidget);
        container2->setObjectName(QStringLiteral("container2"));
        container2->setGeometry(QRect(487, 50, 301, 301));
        sizePolicy.setHeightForWidth(container2->sizePolicy().hasHeightForWidth());
        container2->setSizePolicy(sizePolicy);
        gridLayoutWidget_3 = new QWidget(container2);
        gridLayoutWidget_3->setObjectName(QStringLiteral("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(0, 0, 301, 301));
        grid2 = new QGridLayout(gridLayoutWidget_3);
        grid2->setSpacing(5);
        grid2->setContentsMargins(11, 11, 11, 11);
        grid2->setObjectName(QStringLiteral("grid2"));
        grid2->setSizeConstraint(QLayout::SetDefaultConstraint);
        grid2->setContentsMargins(5, 5, 5, 5);
        g2_case_0_0 = new QLabel(gridLayoutWidget_3);
        g2_case_0_0->setObjectName(QStringLiteral("g2_case_0_0"));
        g2_case_0_0->setFont(font1);
        g2_case_0_0->setAutoFillBackground(false);
        g2_case_0_0->setAlignment(Qt::AlignCenter);
        g2_case_0_0->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_0_0, 0, 0, 1, 1);

        g2_case_0_1 = new QLabel(gridLayoutWidget_3);
        g2_case_0_1->setObjectName(QStringLiteral("g2_case_0_1"));
        g2_case_0_1->setFont(font1);
        g2_case_0_1->setAutoFillBackground(false);
        g2_case_0_1->setAlignment(Qt::AlignCenter);
        g2_case_0_1->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_0_1, 0, 1, 1, 1);

        g2_case_0_2 = new QLabel(gridLayoutWidget_3);
        g2_case_0_2->setObjectName(QStringLiteral("g2_case_0_2"));
        g2_case_0_2->setFont(font1);
        g2_case_0_2->setAutoFillBackground(false);
        g2_case_0_2->setAlignment(Qt::AlignCenter);
        g2_case_0_2->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_0_2, 0, 2, 1, 1);

        g2_case_1_0 = new QLabel(gridLayoutWidget_3);
        g2_case_1_0->setObjectName(QStringLiteral("g2_case_1_0"));
        g2_case_1_0->setFont(font1);
        g2_case_1_0->setAutoFillBackground(false);
        g2_case_1_0->setAlignment(Qt::AlignCenter);
        g2_case_1_0->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_1_0, 1, 0, 1, 1);

        g2_case_0_3 = new QLabel(gridLayoutWidget_3);
        g2_case_0_3->setObjectName(QStringLiteral("g2_case_0_3"));
        g2_case_0_3->setFont(font1);
        g2_case_0_3->setAutoFillBackground(false);
        g2_case_0_3->setAlignment(Qt::AlignCenter);
        g2_case_0_3->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_0_3, 0, 3, 1, 1);

        g2_case_1_2 = new QLabel(gridLayoutWidget_3);
        g2_case_1_2->setObjectName(QStringLiteral("g2_case_1_2"));
        g2_case_1_2->setFont(font1);
        g2_case_1_2->setAutoFillBackground(false);
        g2_case_1_2->setAlignment(Qt::AlignCenter);
        g2_case_1_2->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_1_2, 1, 2, 1, 1);

        g2_case_1_1 = new QLabel(gridLayoutWidget_3);
        g2_case_1_1->setObjectName(QStringLiteral("g2_case_1_1"));
        g2_case_1_1->setFont(font1);
        g2_case_1_1->setAutoFillBackground(false);
        g2_case_1_1->setAlignment(Qt::AlignCenter);
        g2_case_1_1->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_1_1, 1, 1, 1, 1);

        g2_case_1_3 = new QLabel(gridLayoutWidget_3);
        g2_case_1_3->setObjectName(QStringLiteral("g2_case_1_3"));
        g2_case_1_3->setFont(font1);
        g2_case_1_3->setAutoFillBackground(false);
        g2_case_1_3->setAlignment(Qt::AlignCenter);
        g2_case_1_3->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_1_3, 1, 3, 1, 1);

        g2_case_2_0 = new QLabel(gridLayoutWidget_3);
        g2_case_2_0->setObjectName(QStringLiteral("g2_case_2_0"));
        g2_case_2_0->setFont(font1);
        g2_case_2_0->setAutoFillBackground(false);
        g2_case_2_0->setAlignment(Qt::AlignCenter);
        g2_case_2_0->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_2_0, 2, 0, 1, 1);

        g2_case_2_1 = new QLabel(gridLayoutWidget_3);
        g2_case_2_1->setObjectName(QStringLiteral("g2_case_2_1"));
        g2_case_2_1->setFont(font1);
        g2_case_2_1->setAutoFillBackground(false);
        g2_case_2_1->setAlignment(Qt::AlignCenter);
        g2_case_2_1->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_2_1, 2, 1, 1, 1);

        g2_case_2_2 = new QLabel(gridLayoutWidget_3);
        g2_case_2_2->setObjectName(QStringLiteral("g2_case_2_2"));
        g2_case_2_2->setFont(font1);
        g2_case_2_2->setAutoFillBackground(false);
        g2_case_2_2->setAlignment(Qt::AlignCenter);
        g2_case_2_2->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_2_2, 2, 2, 1, 1);

        g2_case_2_3 = new QLabel(gridLayoutWidget_3);
        g2_case_2_3->setObjectName(QStringLiteral("g2_case_2_3"));
        g2_case_2_3->setFont(font1);
        g2_case_2_3->setAutoFillBackground(false);
        g2_case_2_3->setAlignment(Qt::AlignCenter);
        g2_case_2_3->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_2_3, 2, 3, 1, 1);

        g2_case_3_0 = new QLabel(gridLayoutWidget_3);
        g2_case_3_0->setObjectName(QStringLiteral("g2_case_3_0"));
        g2_case_3_0->setFont(font1);
        g2_case_3_0->setAutoFillBackground(false);
        g2_case_3_0->setAlignment(Qt::AlignCenter);
        g2_case_3_0->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_3_0, 3, 0, 1, 1);

        g2_case_3_1 = new QLabel(gridLayoutWidget_3);
        g2_case_3_1->setObjectName(QStringLiteral("g2_case_3_1"));
        g2_case_3_1->setFont(font1);
        g2_case_3_1->setAutoFillBackground(false);
        g2_case_3_1->setAlignment(Qt::AlignCenter);
        g2_case_3_1->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_3_1, 3, 1, 1, 1);

        g2_case_3_2 = new QLabel(gridLayoutWidget_3);
        g2_case_3_2->setObjectName(QStringLiteral("g2_case_3_2"));
        g2_case_3_2->setFont(font1);
        g2_case_3_2->setAutoFillBackground(false);
        g2_case_3_2->setAlignment(Qt::AlignCenter);
        g2_case_3_2->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_3_2, 3, 2, 1, 1);

        g2_case_3_3 = new QLabel(gridLayoutWidget_3);
        g2_case_3_3->setObjectName(QStringLiteral("g2_case_3_3"));
        g2_case_3_3->setFont(font1);
        g2_case_3_3->setAutoFillBackground(false);
        g2_case_3_3->setAlignment(Qt::AlignCenter);
        g2_case_3_3->setProperty("case", QVariant(true));

        grid2->addWidget(g2_case_3_3, 3, 3, 1, 1);

        g2_case_3_0->raise();
        g2_case_2_2->raise();
        g2_case_1_0->raise();
        g2_case_2_3->raise();
        g2_case_0_1->raise();
        g2_case_3_2->raise();
        g2_case_1_1->raise();
        g2_case_2_0->raise();
        g2_case_3_1->raise();
        g2_case_1_2->raise();
        g2_case_3_3->raise();
        g2_case_2_1->raise();
        g2_case_0_0->raise();
        g2_case_1_3->raise();
        g2_case_0_2->raise();
        g2_case_0_3->raise();
        score1 = new QLabel(centralWidget);
        score1->setObjectName(QStringLiteral("score1"));
        score1->setGeometry(QRect(62, 380, 300, 30));
        QFont font2;
        font2.setFamily(QStringLiteral("Generica"));
        font2.setPointSize(30);
        score1->setFont(font2);
        score1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        score1_2 = new QLabel(centralWidget);
        score1_2->setObjectName(QStringLiteral("score1_2"));
        score1_2->setGeometry(QRect(487, 380, 300, 30));
        score1_2->setFont(font2);
        score1_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 850, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "2048 Battle", 0));
        J1->setText(QApplication::translate("MainWindow", "Joueur 1", 0));
        J2->setText(QApplication::translate("MainWindow", "Joueur 2", 0));
        g1_case_0_0->setText(QString());
        g1_case_0_1->setText(QString());
        g1_case_0_2->setText(QString());
        g1_case_1_0->setText(QString());
        g1_case_0_3->setText(QString());
        g1_case_1_2->setText(QString());
        g1_case_1_1->setText(QString());
        g1_case_1_3->setText(QString());
        g1_case_2_0->setText(QString());
        g1_case_2_1->setText(QString());
        g1_case_2_2->setText(QString());
        g1_case_2_3->setText(QString());
        g1_case_3_0->setText(QString());
        g1_case_3_1->setText(QString());
        g1_case_3_2->setText(QString());
        g1_case_3_3->setText(QString());
        g2_case_0_0->setText(QString());
        g2_case_0_1->setText(QString());
        g2_case_0_2->setText(QString());
        g2_case_1_0->setText(QString());
        g2_case_0_3->setText(QString());
        g2_case_1_2->setText(QString());
        g2_case_1_1->setText(QString());
        g2_case_1_3->setText(QString());
        g2_case_2_0->setText(QString());
        g2_case_2_1->setText(QString());
        g2_case_2_2->setText(QString());
        g2_case_2_3->setText(QString());
        g2_case_3_0->setText(QString());
        g2_case_3_1->setText(QString());
        g2_case_3_2->setText(QString());
        g2_case_3_3->setText(QString());
        score1->setText(QApplication::translate("MainWindow", "0", 0));
        score1_2->setText(QApplication::translate("MainWindow", "0", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
